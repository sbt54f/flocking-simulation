from bird import Bird

class Dummy(Bird):
    def __init__(self, x, y, bird_id, p_std=1, v_std=1):
        super().__init__(x, y, bird_id, p_std=p_std, v_std=v_std)

    def calculate_v(self, w):
        self.time_since_target += 1
        if w.target.length_squared() > 0.0:
            return (w.target.normalize() * (Bird.max_speed))
        else:
            return w.target
