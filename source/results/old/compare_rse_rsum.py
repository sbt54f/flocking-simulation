import numpy as np

from source.layouts import EmptyWorld


noises = np.linspace(0, 4, 21)
print(noises)

folder = 'data/rse/adaptive3/'
asd = 0
for noise in noises:
    w = EmptyWorld(100, 10, attack_power=noise)
    w.change_noise(noise, noise)

    rsum_file = 'rsum_' + "{:.2f}".format(noise) + '.dat'
    rse_file = 'rse_' + "{:.2f}".format(noise) + '.dat'

    rsums = [ [] for _ in range(len(w.birds)) ]
    RSEs = [ [] for _ in range(len(w.birds)) ]

    # for repeat in range(0, 3):
    iterations = 1000
    for i in range(0, iterations):
        w.update(0.02)
        step = []
        for b in w.birds:
            rsums[b.id].append(b.rsum.length())
            RSEs[b.id].append(b.RSE)

        print(asd, i)


    with open(folder+rsum_file, "w") as f:
        for i in range(0, iterations):
            line = ''
            for b in range(0, len(w.birds)):
                line += str(rsums[b][i]) + ','
            f.write(line[:-1])
            f.write('\n')

    with open(folder+rse_file, "w") as f:
        for i in range(0, iterations):
            line = ''
            for b in range(0, len(w.birds)):
                line += str(RSEs[b][i]) + ','
            f.write(line[:-1])
            f.write('\n')

    asd += 1