import matplotlib.pyplot as plt

folder = 'data/successrates/'
file = 'data_4.1.dat'




noises = []
times = []

with open(folder+file, 'r') as f:

    line = f.readline()
    while line != '':
        results = line.strip().split(',')
        print(results)
        for r in range(1, len(results)):
            noises.append(float(results[0]))
            times.append(float(results[r]))
        line = f.readline()

plt.scatter(noises, times)
plt.ylim(0)
plt.show()

