import matplotlib.pyplot as plt
import numpy as np
from os import listdir

fig = plt.figure()
ax = fig.add_subplot(projection='3d')

def bound(n, sigma):
    return 1.96 * np.sqrt(n) * sigma


in_xs = []
in_ys = []
in_zs = []


out_xs = []
out_ys = []
out_zs = []

root = 'data/predict/rsum/'
files = listdir(root)

max_r = 0
for file in files:

    with open(root+file, 'r') as f:
        line = f.readline().strip()
        values = line.split(',')
        noise = float(values[0])
        avg_r = float(values[1])
        if avg_r > max_r:
            max_r = avg_r
        line = f.readline().strip()
        while line:
            values = line.split(',')
            x = float(values[0])
            y = float(values[1])
            # in_xs.append(float(values[0]))
            # in_ys.append(float(values[1]))
            # in_zs.append(noise)
            if np.sqrt(x**2 + y**2) > 1.96*np.sqrt(noise)*np.sqrt(200):
                out_xs.append(float(values[0]))
                out_ys.append(float(values[1]))
                out_zs.append(noise)
            else:
                in_xs.append(float(values[0]))
                in_ys.append(float(values[1]))
                in_zs.append(noise)

            line = f.readline().strip()




print(len(in_xs), len(out_xs), len(out_xs)/(len(out_xs)+len(in_xs)))

ax.scatter(in_xs, in_ys, in_zs, alpha=.1, color='blue', label='points inside expected interval')
ax.scatter(out_xs, out_ys, out_zs, alpha=0.5, color='red', marker='x', label='points outside expected interval')

ax.set_zlim(0,0.3)
ax.set_xlim(-10, 10)
ax.set_ylim(-10, 10)

ax.set_title('Cone of 95% confidence interval',fontdict={'fontsize': 20})

ax.set_xlabel('X error')
ax.set_ylabel('Y error')
ax.set_zlabel('Noise')


# fig = plt.figure()
# ax = fig.add_subplot(projection='3d')
# plt.show()

#
# X = np.array()
# Y = np.array()


# def f(x, y):
#     return np.sin(np.sqrt(x ** 2 + y ** 2))
#
# theta = 2 * np.pi * np.random.random(1000)
# r = 6 * np.random.random(1000)
# x = np.ravel(r * np.sin(theta))
# y = np.ravel(r * np.cos(theta))
# z = f(x, y)



predicted_x = np.zeros(20*40)
predicted_y = np.zeros(20*40)
predicted_z = np.zeros(20*40)

i = 0
for noise in np.linspace(0, 0.3, 20):
    confidence = 1.96*np.sqrt(noise)*np.sqrt(200)
    for theta in np.linspace(0, 2*np.pi, 40):
        predicted_x[i] = confidence*np.cos(theta)
        predicted_y[i] = confidence*np.sin(theta)
        predicted_z[i] = noise
        i = i+1

# ax.plot_trisurf(predicted_x, predicted_y, predicted_z,
#                 cmap='viridis', edgecolor='none')
plt.legend(loc=1, prop={'size': 20})

plt.show()

# X, Y = np.meshgrid(X, Y)
#
# # Plot the surface.
#
# Z = np.array(Z)
#
# fig, ax = plt.subplots(subplot_kw={"projection": "3d"})
# surf = ax.plot_surface(X, Y, Z, cmap=cm.coolwarm,
#                        linewidth=0, antialiased=False)
#
# # Customize the z axis.
# # ax.zaxis.set_major_locator(LinearLocator(10))
# # A StrMethodFormatter is used automatically
# ax.zaxis.set_major_formatter('{x:.02f}')
#
# # Add a color bar which maps values to colors.
# fig.colorbar(surf, shrink=0.5, aspect=5)
#
# if title:
#     ax.set_title( title )
