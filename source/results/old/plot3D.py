import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator

def plot_heatmap(X, Y, Z, title=None):
    fig, ax = plt.subplots()

    X_old = X
    Y_old = Y

    # Make data.
    X = np.array(X)
    Y = np.array(Y)
    X, Y = np.meshgrid(X, Y)

    # Plot the surface.

    Z = np.array(Z)
    c = ax.pcolormesh(X, Y, Z, cmap='RdBu')
    ax.set_title('pcolormesh')
    # set the limits of the plot to the limits of the data
    ax.axis([X.min(), X.max(), Y.min(), Y.max()])
    fig.colorbar(c, ax=ax)

    ax.set_ylabel('Power')
    ax.set_xlabel('Noise')

    if title:
        ax.set_title( title )

    return fig, ax

def plot_surface(X, Y, Z, title=None):
    X = np.array(X)
    Y = np.array(Y)
    X, Y = np.meshgrid(X, Y)

    # Plot the surface.

    Z = np.array(Z)

    fig, ax = plt.subplots(subplot_kw={"projection": "3d"})
    surf = ax.plot_surface(X, Y, Z, cmap=cm.coolwarm,
                           linewidth=0, antialiased=False)

    # Customize the z axis.
    # ax.zaxis.set_major_locator(LinearLocator(10))
    # A StrMethodFormatter is used automatically
    ax.zaxis.set_major_formatter('{x:.02f}')

    # Add a color bar which maps values to colors.
    fig.colorbar(surf, shrink=0.5, aspect=5)

    if title:
        ax.set_title( title )

    return fig, ax
