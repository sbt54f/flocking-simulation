import sys
sys.path.append("/home/simon/Desktop/flocker/flocking-simulation/venv/lib/python3.9/site-packages/")
sys.path.append("/home/simon/Desktop/flocker/flocking-simulation/")
import numpy as np
from source.layouts import SteadyWorld
import random
from decimal import Decimal

id = sys.argv[1]

normals = 50
intruders = 0
power = 1

iterations = 10000
rsum_xs = []
rsum_ys = []

rses = []

noise = random.random()*0.3
noise = (float(id)/100)*0.3

w = SteadyWorld(normals, intruders, attack_power=power)
w.reset_error_sums()
w.change_noise(noise, noise)

average_r = Decimal(0.0)
for i in range(iterations):
    w.update(0.02)
    for b in w.birds:
        average_r += Decimal(b.error)

for b in w.birds:
    rsum_xs.append(b.rsum.x)
    rsum_ys.append(b.rsum.y)
    rses.append(b.RSE)

average_r = float(average_r/(iterations*len(w.birds)))

folder = 'data/predict/rsum/'
file = f'batch{id}.dat'


with open(folder+file, 'w')as f:
    f.write(f'{noise},{average_r}\n')
    for point in range(len(rsum_xs)):
        f.write(f'{rsum_xs[point]},{rsum_ys[point]}\n')

folder = 'data/predict/rse/'
file = f'batch{id}.dat'


with open(folder+file, 'w')as f:
    f.write(f'{noise},{average_r}\n')
    for point in range(len(rses)):
        f.write(f'{rses[point]}\n')