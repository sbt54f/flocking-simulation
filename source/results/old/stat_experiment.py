import math
import random
from os import listdir
import matplotlib.pyplot as plt


def gen_rsum(n, sigma):
    rsum = 0.0
    for i in range(n):
        rsum += random.normalvariate(0, sigma)
    return rsum


n = 100
sigma = 0.1


results = []

for i in range(n):
    results.append(gen_rsum(n, sigma))

predicted = 1.96*math.sqrt(n)*sigma
print(predicted)
count = 0
for r in results:
    if abs(r) > predicted:
        count += 1

print(count)
print(sum(results)/len(results))

root = 'data/predict/rsum/'
root2 = 'data/predict/rse/'

files = listdir(root2)

xs = []
ys = []
ratios = []

max_r = 0
max_mag = 0
for file in files:
    xs = []
    ys = []
    colors = []
    with open(root+file, 'r') as f:
        line = f.readline().strip()
        values = line.split(',')
        noise = float(values[0])
        avg_r = float(values[1])
        #.95 = 1.96
        #.99 = 2.576
        predicted = 1.96 * math.sqrt(10000) * avg_r

        total = 0
        out_count = 0

        if avg_r > max_r:
            max_r = avg_r
        line = f.readline().strip()
        while line:
            values = line.split(',')
            x = float(values[0])
            y = float(values[1])
            xs.append(x)
            ys.append(y)

            mag = math.sqrt(x**2 + y**2)
            if mag > predicted:
                out_count += 1
            total += 1

            line = f.readline().strip()



        ratio = out_count / total
        print(ratio, out_count, total)
        ratios.append(ratio)
        # plt.scatter(xs, ys)
        
        # bounds = plt.Circle((0,0), predicted, fill=False)
        # plt.gca().add_patch(bounds)
        # limit = predicted*1.2
        # plt.xlim(-limit, limit)
        # plt.ylim(-limit, limit)
        # plt.show()

    with open(root2+file, 'r') as f:
        rses = []
        line = f.readline().strip()
        values = line.split(',')
        noise = float(values[0])
        avg_r = float(values[1])
        line = f.readline().strip()
        while line:
            value = float(line.strip())
            rses.append(value)
            line = f.readline().strip()

        standard_deviation = avg_r*math.sqrt(10000)
        predicted = 10000*(standard_deviation**2)
        print(predicted, sum(rses)/len(rses))



print(max_mag)
print(sum(ratios)/len(ratios))




#
#
# plt.scatter(xs, ys, c=colors)
# plt.xlim(-20, 20)
# plt.ylim(-20, 20)

plt.show()