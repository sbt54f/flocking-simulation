import matplotlib.pyplot as plt
import numpy as np

subfolder = 'data/rsum_stats'
filename = subfolder + "/seeker_high_noise.dat"

rsums = []

avg_good = []
avg_bad = []

with open(filename, 'r') as f:
    line = f.readline()
    while line != '':
        values = line.split(',')
        if len(rsums) == 0:
            rsums = [[] for _ in range(len(values))]

        avg_good.append(0)
        avg_bad.append(0)

        for v in range(0, len(values)):
            value = float(values[v])
            rsums[v].append(value)
            if v < 100:
                avg_good[-1]+=value
            else:
                avg_bad[-1]+=value

        avg_good[-1] /= 100
        avg_bad[-1] /= 10

        line = f.readline()



xs = np.arange(0, len(rsums[0]))

color = 'grey'

for b in range(0, len(rsums)):
    if b >=100:
        color = (213/255,94/255,0)
    plt.plot(xs, rsums[b], color=color)
plt.plot(xs, avg_good, color='black', linewidth=4, label='avg normal')
plt.plot(xs, avg_bad, color=(213/255,94/255,0), linewidth=4, label='avg intruder')

plt.ylim(0, 25)
plt.xlim(0, 1000)

plt.title('High Noise')
plt.legend()
plt.xlabel('Time')
plt.ylabel('Residual Sum')

plt.figure()

subfolder = 'data/rsum_stats'
filename = subfolder + "/seeker_low_noise.dat"

rsums = []

avg_good = []
avg_bad = []

with open(filename, 'r') as f:
    line = f.readline()
    while line != '':
        values = line.split(',')
        if len(rsums) == 0:
            rsums = [[] for _ in range(len(values))]

        avg_good.append(0)
        avg_bad.append(0)

        for v in range(0, len(values)):
            value = float(values[v])
            rsums[v].append(value)
            if v < 100:
                avg_good[-1]+=value
            else:
                avg_bad[-1]+=value

        avg_good[-1] /= 100
        avg_bad[-1] /= 10

        line = f.readline()



xs = np.arange(0, len(rsums[0]))

color = 'grey'

for b in range(0, len(rsums)):
    if b >=100:
        color = (213/255,94/255,0)
    plt.plot(xs, rsums[b], color=color)
plt.plot(xs, avg_good, color='black', linewidth=4)
plt.plot(xs, avg_bad, color=(213/255,94/255,0), linewidth=4)

plt.ylim(0, 45)
plt.xlim(0, 1000)
plt.title('Low Noise')

plt.show()
# plt.scatter(xs, rsums[0])
# plt.show()
# for b in range(0, 5):
# plt.plot(np.arange(0, len(rsums[0])), rsums[0])
#
# plt.show()