import matplotlib.pyplot as plt
import numpy as np
import os
import re


folder = 'data/rse/adaptive3/'
filenames = sorted(os.listdir(folder))

rse_good_avgs = []
rse_bad_avgs = []
rsum_good_avgs = []
rsum_bad_avgs = []

noises = []


for filename in filenames:
    parts = filename.split('_')
    value = float(parts[1][0:-4])
    bad_avg = 0
    good_avg = 0
    with open(folder + filename, 'r') as f:
        line = f.readline()
        while line != '':
            values = line.split(',')
            for i in range(0, 100):
                good_avg += float(values[i])
            for i in range(100, len(values)):
                bad_avg += float(values[i])
            line = f.readline()
    if parts[0] == 'rse':
        rse_good_avgs.append((value, good_avg/100))
        rse_bad_avgs.append((value, bad_avg/10))
        noises.append(value)
    else:
        rsum_good_avgs.append((value, good_avg/100))
        rsum_bad_avgs.append((value, bad_avg/10))

    print(parts[0], value, good_avg, bad_avg)

rsum_good_avgs = sorted(rsum_good_avgs, key= lambda x: x[0])
rsum_good_avgs = list((x[1] for x in rsum_good_avgs))

rsum_bad_avgs = sorted(rsum_bad_avgs, key= lambda x: x[0])
rsum_bad_avgs = list((x[1] for x in rsum_bad_avgs))

rse_good_avgs = sorted(rse_good_avgs, key= lambda x: x[0])
rse_good_avgs = list((x[1] for x in rse_good_avgs))

rse_bad_avgs = sorted(rse_bad_avgs, key= lambda x: x[0])
rse_bad_avgs = list((x[1] for x in rse_bad_avgs))

noises = sorted(noises)

plt.plot(noises, rsum_good_avgs, label='rsum normal', color=(213/255,94/255,0/255), linestyle='--')
plt.plot(noises, rsum_bad_avgs, label='rsum intruder', color=(213/255,94/255,0/255))

plt.plot(noises, rse_good_avgs, label='RSS normal', color='black', linestyle='--')
plt.plot(noises, rse_bad_avgs, label='RSS intruder', color='black')

plt.xlabel('Noise')
plt.ylabel('Average Error')

plt.legend()
plt.show()


#
# for file in os.listdir(folder):
#     parts = file.split('_')
#     value = "{:.2f}".format(float(parts[1][0:-4]))
#     new_file = parts[0] + '_' + value + parts[1][-4:]
#     os.rename(folder+file, folder+new_file)
#     print(file, value, new_file)
#
# file = 'high_noise_rse.dat'
# file2 = 'low_noise_rse.dat'
#
# rsums = []
# RSEs = []
#
# avg_good = []
# avg_bad = []
#
# with open(folder+ file, 'r') as f:
#     line = f.readline()
#     while line != '':
#         values = line.split(',')
#         if len(rsums) == 0:
#             rsums = [[] for _ in range(len(values))]
#
#
#         avg_good.append(0)
#         avg_bad.append(0)
#
#         for v in range(0, len(values)):
#             value = float(values[v])
#             rsums[v].append(value)
#             if v < 100:
#                 avg_good[-1]+=value
#             else:
#                 avg_bad[-1]+=value
#
#         avg_good[-1] /= 100
#         avg_bad[-1] /= 10
#
#         line = f.readline()
#
# with open(folder+file2, 'r') as f:
#     line = f.readline()
#     while line != '':
#         values = line.split(',')
#         if len(RSEs) == 0:
#             RSEs = [[] for _ in range(len(values))]
#
#
#         for v in range(0, len(values)):
#             value = float(values[v])
#             RSEs[v].append(value)
#
#         line = f.readline()
#
# xs = np.arange(0, len(rsums[0]))
#
# color = 'grey'
#
# for b in range(100, len(rsums)):
#     if b >=100:
#         color = (213/255,94/255,0)
#     plt.plot(xs, RSEs[b], color=color)
# for b in range(100, len(rsums)):
#     if b >= 100:
#         color = (213 / 255, 94 / 255, 0)
#     plt.plot(xs, rsums[b], color='green')
# plt.plot(xs, avg_good, color='black', linewidth=4)
# plt.plot(xs, avg_bad, color=(213/255,94/255,0), linewidth=4)
#
# plt.xlim(0, 1000)
# plt.title('Low Noise')
#
# plt.show()
