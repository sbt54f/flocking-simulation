import matplotlib.pyplot as plt
import numpy as np
from os import listdir

def get_success_heat_data(root):
    folder = root

    files = sorted(listdir(folder))

    data = {}


    for file in files:
        power = float(file[6:-4])
        results = []
        with open(folder+file, 'r') as f:
            line = f.readline()
            noises = []

            while line != '':
                values = line.split(',')
                avg = 0
                count = 0
                noises.append(float(values[0]))
                for v in values[1:]:
                    if v != '\n':
                        avg += float(v)
                        count += 1
                results.append(avg/count)
                line = f.readline()
        data[power] = results

    powers = []
    Z = []
    for key in sorted(data.keys()):
        Z.append(data[key])
        powers.append(key)


    Z = np.array(Z)

    return noises, powers, Z


# x, y = np.meshgrid(np.linspace(min(powers), max(powers), len(powers)), np.linspace(min(noises), max(noises), len(noises)))
#
# fig, ax = plt.subplots()
#
# c = ax.pcolormesh(x, y, Z, cmap='RdBu')
# ax.set_title('pcolormesh')
# # set the limits of the plot to the limits of the data
# ax.axis([x.min(), x.max(), y.min(), y.max()])
# fig.colorbar(c, ax=ax)
#
# plt.show()

