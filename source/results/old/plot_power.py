import os
import numpy as np
import matplotlib.pyplot as plt



def get_residual_results(root):
    folders = os.listdir(root)

    powers = []
    for folder in folders:
        powers.append(float(folder[6:]))

    powers = sorted(powers)

    files = os.listdir(root+folders[0])
    noises = []
    for file in files:
        noises.append(float(file[6:10]))
    noises = sorted(noises)

    normal_zs = np.zeros((len(powers), len(noises)))
    attacker_zs = np.zeros((len(powers), len(noises)))

    for p in range(len(powers)):
        power = powers[p]
        folder = 'power_' + '%.1f' % power
        for n in range(len(noises)):
            noise = noises[n]
            file = 'noise_' + '%.2f' % noise + '.dat'
            with open(root + folder + '/' + file, 'r') as f:
                averages = []
                line = f.readline().strip()
                while line != '':
                    values = line.split(',')
                    avg = 0
                    for v in values[:-1]:
                        avg += float(v)
                    avg /= len(values)
                    averages.append(avg)
                    line = f.readline().strip()

                attacker_rse = averages[-1]
                averages = averages[:-1]
                normal_rse = sum(averages)/len(averages)
                normal_zs[p, n] = normal_rse
                attacker_zs[p, n] = attacker_rse

    print(noises)
    return noises, powers, normal_zs, attacker_zs

# fig, ax = plt.subplots()
#
# # Make data.
# X = np.array(powers)
# Y = np.array(noises)
# X, Y = np.meshgrid(X, Y)
#
# # Plot the surface.
#
# Z = np.subtract(attacker_zs.T, normal_zs.T)
# # Z = np.divide(attacker_zs.T, Z)
#
# c = ax.pcolormesh(X, Y, Z, cmap='RdBu')
# ax.set_title('pcolormesh')
# # set the limits of the plot to the limits of the data
# ax.axis([X.min(), X.max(), Y.min(), Y.max()])
# fig.colorbar(c, ax=ax)
#
# ax.set_ylabel('Noise')
# ax.set_xlabel('power')


