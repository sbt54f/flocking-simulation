import sys
import os
print(os.getcwd())
sys.path.append("/home/simon/Desktop/flocker/flocking-simulation/venv/lib/python3.9/site-packages/")
sys.path.append("/home/simon/Desktop/flocker/flocking-simulation/")

import numpy as np
import pygame

from source.layouts import EmptyWorld, SteadyWorld
from source.watchdog import Watchdog


power = (float(sys.argv[1]) + 1) / 2


max_iterations = 10000

result_times = []
result_rsums = []
result_rse = []
result_rsum_at_success = []
result_rse_at_success = []


noises = np.linspace(0.0, 1, 21)

normals = 50
intruders = 1

rep_count = 3

for noise in noises:

    times = []
    rsums = []
    rses = []
    rsums_as = []
    rses_as = []
    for rep in range(0, rep_count):
        w = SteadyWorld(normals, intruders, attack_power=power)
        w.birds[50].p = pygame.Vector2(0, 180)
        w.update(0.02)
        w.change_noise(noise, noise)
        watchdog = Watchdog()

        intruder = w.birds[normals]

        d = intruder.get_distance_to_target(w)

        i = 1
        while i < max_iterations and not watchdog.successful_attack:
            w.update(0.02)
            watchdog.watch(w)
            if i % 1000 == 0:
                new_d = intruder.get_distance_to_target(w)
                if d-new_d < 1:
                    i = max_iterations
                else:
                    d = new_d
            i += 1
        times.append(i)
        all_rsums = []
        all_rses = []
        for b in w.birds:
            all_rsums.append(b.rsum.length())
            all_rses.append(b.RSE)
        rsums_as.append(all_rsums)
        rses_as.append(all_rses)

        while i < max_iterations:
            w.update(0.02)
            i += 1

        all_rsums = []
        all_rses = []
        for b in w.birds:
            all_rsums.append(b.rsum.length())
            all_rses.append(b.RSE)
        rsums.append(all_rsums)
        rses.append(all_rses)

        print(power, noise, rep, i)
    result_times.append([noise, times])
    result_rsums.append([noise, rsums])
    result_rse.append([noise, rses])
    result_rsum_at_success.append([noise, rsums_as])
    result_rse_at_success.append([noise, rses_as])

# 5 2 1 51

success_folder = 'data/heat2/success/'
success_file = 'power_' + str(power) + '.dat'


with open(success_folder+success_file, 'w') as f:
    for r in result_times:
        f.write(str(r[0]))
        for e in r[1]:
            f.write(',' + str(e))
        f.write('\n')

def save_residual_data(root, data):
    if not os.path.exists(root):
        os.makedirs(root)

    for i in range(0, len(data)):
        noise = data[i][0]
        file = 'noise_' + '%.2f' % noise + '.dat'
        with open(root + file, 'w') as f:
            for b in range(len(w.birds)):
                for rep in range(rep_count):
                    f.write(str(data[i][1][rep][b]) + ',')
                f.write('\n')

rsum_folder = 'data/heat2/rsum/power_' + str(power) + '/'
save_residual_data(rsum_folder, result_rsums)

rsum_folder = 'data/heat2/rsum_as/power_' + str(power) + '/'
save_residual_data(rsum_folder, result_rsum_at_success)

rse_folder = 'data/heat2/rse/power_' + str(power) + '/'
save_residual_data(rse_folder, result_rse)

rse_folder = 'data/heat2/rse_as/power_' + str(power) + '/'
save_residual_data(rse_folder, result_rse_at_success)

