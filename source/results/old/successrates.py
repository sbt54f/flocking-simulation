import copy

import numpy as np
import pygame

from source.layouts import EmptyWorld
from source.watchdog import Watchdog


power = 4.1

folder = 'data/heat/success/'
file = 'data_' + str(power) + '.dat'

rep_count = 3
max_iterations = 10000

results = []
noises = np.linspace(0.4, 0.6, 21)

normals = 50
intruders = 1


for noise in noises:
    times = []
    for rep in range(0, rep_count):
        w = EmptyWorld(normals, intruders, attack_power=power)
        w.birds[50].p = pygame.Vector2(0, 180)
        w.change_noise(noise, noise)
        watchdog = Watchdog()

        i = 0
        while i < max_iterations and not watchdog.successful_attack:
            w.update(0.02)
            watchdog.watch(w)
            i += 1
        print(noise, rep, i)

        times.append(i)
    results.append([noise, times])


with open(folder+file, 'w') as f:
    for r in results:

        f.write(str(r[0]))
        for e in r[1]:
            f.write(',' + str(e))
        f.write('\n')



