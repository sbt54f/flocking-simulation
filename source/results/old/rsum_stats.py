from source.world import *
from source.layouts import EmptyWorld
import matplotlib.pyplot as plt
import numpy as np
from source.olafi_flocking import *
import os

w = EmptyWorld(100, 10)
w.change_noise(2.10, 2.10)

rsums = [ [] for _ in range(len(w.birds)) ]


iterations = 1000
for i in range(0, iterations):
    w.update(0.02)
    step = []
    for b in w.birds:
        rsums[b.id].append(b.rsum.length())
    # if i >= 500:
    for n in range(100, len(w.birds)):
        bird = w.birds[n]
        if (bird.formation_target - bird.p).length() <= 10:
            w.birds[n].mode = AttackMode.RESET
            print(n, bird.mode)
    print(i)

for n in range(100, len(w.birds)):
    print(w.birds[n].mode)

subfolder = 'data/rsum_stats'
filename = subfolder + "/seeker_high_noise.dat"

if not os.path.isdir(subfolder):
    os.mkdir(subfolder)

with open(filename, "w") as f:
    for i in range(0, iterations):
        line = ''
        for b in range(0, len(w.birds)):
            line += str(rsums[b][i]) + ','
        f.write(line[:-1])
        f.write('\n')