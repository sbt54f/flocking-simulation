from plot_success_heat import get_success_heat_data
from plot_power import get_residual_results
from plot3D import plot_surface, plot_heatmap
import numpy as np

import matplotlib.pyplot as plt

_, _, success = get_success_heat_data('data/heat2/success/')
ns, ps, rsum_normal, rsum_attack = get_residual_results('data/heat2/rsum/')
_, _, rsum_normal_as, rsum_attack_as = get_residual_results('data/heat2/rsum_as/')
ns, ps, rse_normal, rse_attack = get_residual_results('data/heat2/rse/')
_, _, rse_normal_as, rse_attack_as = get_residual_results('data/heat2/rse_as/')

#
# plot_surface(ns, ps, rse_normal, title='rse normal')
# plot_surface(ns, ps, rse_attack, title='rse attack')


threshold_curve = []

for n in range(len(ns)):
    for p in range(len(ps)):
        if success[p][n] < 10000:
            threshold_curve.append(ps[p-1])
            break

rsum_as_ratio = np.divide(rsum_attack_as, rsum_normal_as)
rse_as_ratio = np.divide(rse_attack_as, rse_normal_as)



for r in range(len(rsum_as_ratio)):
    for r2 in range(len(rsum_as_ratio[r])):
        if rsum_as_ratio[(r, r2)] > 15:
            rsum_as_ratio[(r, r2)] = 15
        if rse_as_ratio[(r, r2)] > 1:
            rse_as_ratio[(r, r2)] = 1

plot_heatmap(ns, ps, success, title='success')

plot_heatmap(ns, ps, rsum_as_ratio, title = 'rsum ratio')
plot_heatmap(ns, ps, rse_as_ratio, title = 'rse ratio')

plot_surface(ns, ps, rsum_normal_as, title='rsum normal')
plot_surface(ns, ps, rse_normal_as, title='rse normal')



# fig, ax = plot_heatmap(ns, ps, rsum_attack-rsum_normal, title='rsum at success')
# ax.plot(ns, threshold_curve, linewidth=4, color = 'black')
#
# fig, ax = plot_heatmap(ns, ps, rse_attack-rse_normal, title='rsum')
# ax.plot(ns, threshold_curve, linewidth=4, color = 'black')



plt.show()
