import matplotlib.pyplot as plt
import numpy as np
import os

folder = 'data/over_time/'

run_count = int(sorted(os.listdir(folder))[-1].split('_')[1])

folder += 'run_' + str(run_count) + '/'

times = []
g_rsum = []
b_rsum = []
g_rss = []
b_rss = []
successes = []

print(folder)

with open(folder + 'good_rsum.dat', 'r') as f:
    line = f.readline()
    while line != '':
        values = line.split(',')
        times.append(float(values[0]))
        current = []
        for i in range(1, len(values)):
            current.append(float(values[i]))
        g_rsum.append(current)
        line = f.readline()

g_rsum = np.array(g_rsum).transpose()

with open(folder + 'bad_rsum.dat', 'r') as f:
    line = f.readline()
    while line != '':
        values = line.split(',')
        current = []
        for i in range(1, len(values)):
            current.append(float(values[i]))
        b_rsum.append(current)
        line = f.readline()

b_rsum = np.array(b_rsum).transpose()

with open(folder + 'good_rss.dat', 'r') as f:
    line = f.readline()
    while line != '':
        values = line.split(',')
        current = []
        for i in range(1, len(values)):
            current.append(float(values[i]))
        g_rss.append(current)
        line = f.readline()

g_rss = np.array(g_rss).transpose()

with open(folder + 'bad_rss.dat', 'r') as f:
    line = f.readline()
    while line != '':
        values = line.split(',')
        current = []
        for i in range(1, len(values)):
            current.append(float(values[i]))
        b_rss.append(current)
        line = f.readline()

b_rss = np.array(b_rss).transpose()

with open(folder + 'success.dat', 'r') as f:
    line = f.readline()
    while line != '':
        # times.append(int(values[0]))
        successes.append(float(line))

        line = f.readline()

# successes = np.multiply(successes, 50)
# plt.plot(times, successes, color='green')
#
for b in range(g_rsum.shape[0]):
    plt.plot(times, g_rsum[b], color='grey', label='regular', linewidth=0.5)

for b in range(b_rsum.shape[0]):
    plt.plot(times, b_rsum[b], color='black', label='intruder', linewidth=2)

handles, labels = plt.gca().get_legend_handles_labels()
labels, ids = np.unique(labels, return_index=True)
handles = [handles[i] for i in ids]
plt.legend(handles, labels, loc='best')
plt.title('RSUM')
plt.xlabel('time')
plt.ylabel('RSUM')
plt.figure()

for b in range(g_rss.shape[0]):
    plt.plot(times, g_rss[b], color='grey', label='regular', linewidth=0.5)

for b in range(b_rss.shape[0]):
    plt.plot(times, b_rss[b], color='black', label='intruder', linewidth=2)

handles, labels = plt.gca().get_legend_handles_labels()
labels, ids = np.unique(labels, return_index=True)
handles = [handles[i] for i in ids]
plt.legend(handles, labels, loc='best')

plt.title('RSS')
plt.xlabel('time')
plt.ylabel('RSS')
# plt.plot(times, g_rss, label='good rss')
# plt.plot(times, b_rss, label='bad rss')


plt.show()