import os
os.environ['PYGAME_HIDE_SUPPORT_PROMPT'] = "hide"
import sys
sys.path.append("/home/simon/Desktop/flocker/flocking-simulation/venv/lib/python3.9/site-packages/")
sys.path.append("/home/simon/Desktop/flocker/flocking-simulation/")
import source.world
import source.layouts
import source.olafi_flocking
import numpy as np
from source.watchdog import Watchdog

folder = 'data/success_heat/'



args = sys.argv[0]

t_max = 5000
good_count = 50
bad_count = 1
if len(sys.argv) > 1:
    p_error = float(sys.argv[1])/20
    v_error = float(sys.argv[2])/20
    attack_power = float(sys.argv[3])/2
    folder += sys.argv[4]
    if not os.path.isdir(folder):
        os.mkdir(folder)
else:
    p_error = 1
    v_error = 1
    attack_power = 5
    folder += 'run_1/'
    if not os.path.isdir(folder):
        os.mkdir(folder)

w = source.layouts.SteadyWorld(good_count, bad_count, attack_power)
w.change_noise(p_error, v_error)

watcher = Watchdog()

success_time = t_max
print("started " + str(sys.argv[1]) + ',' + str(sys.argv[3]))

# Actual simulation. At each timestep we save RSUM and RSS information
for t in range(t_max):
    if t % 100 == 0 and len(sys.argv) == 1:
        print(t)
    w.update(0.05)
    watcher.watch(w)
    if watcher.successful_attack:
        success_time = t
        break


print("ended " + str(sys.argv[1]) + ',' + str(sys.argv[3]))

rsum_good = 0.0
rsum_bad = 0.0
rss_good = 0.0
rss_bad = 0.0

rsum_max = 0.0
rss_max = 0.0

for b in w.birds:
    if type(b) == source.olafi_flocking.OlfatiIntruder:
        rsum_bad += b.rsum.length()
        rss_bad += b.rss
    else:
        l = b.rsum.length()
        rsum_good += l
        if l > rsum_max:
            rsum_max = l
        rss_good += b.rss
        if b.rss > rss_max:
            rss_max = b.rss
#
rsum_good /= good_count
rsum_bad /= bad_count
rss_good /= good_count
rss_bad /= bad_count

folder += 'error_' + str(p_error) + '/'
if not os.path.isdir(folder):
    os.mkdir(folder)

folder += 'power_' + str(attack_power) + '/'

if not os.path.isdir(folder):
    os.mkdir(folder)

# success_rate = sum(successes)/len(successes)

print(folder+'data.dat')

with open(folder+'data.dat', 'w') as f:
    f.write(str(success_time) + ',' + str(rsum_good) + ',' + str(rsum_bad) + ',' + str(rsum_max) + ',' + str(rss_good) + ',' + str(rss_bad) + ',' + str(rss_max))



