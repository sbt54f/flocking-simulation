import os
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm

folder = 'data/noise_power_ratio/'

run_count = int(sorted(os.listdir(folder))[-1].split('_')[1])

folder += 'run_' + str(run_count) + '/'


rsum_good = np.zeros((21, 21))
rsum_bad = np.zeros((21, 21))
success_rate = np.zeros((21, 21))

rss_good = np.zeros((21, 21))
rss_bad = np.zeros((21, 21))

xs = []
ys = []

for error_file in os.listdir(folder):
    error = int(float(error_file.split('_')[1])*4)
    if len(xs) < 21:
        xs.append(error/4)
    for power_file in os.listdir(folder + error_file):
        power = int(float(power_file.split('_')[1]))
        if len(ys) < 21:
            ys.append(power)
        with open(folder + error_file + '/' + power_file + '/data.dat', 'r') as f:
            values = f.readline().split(',')
            rsum_good[power, error] = float(values[0])
            rsum_bad[power, error] = float(values[1])
            success_rate[power, error] = float(values[2])
            values = f.readline().split(',')
            rss_good[power, error] = float(values[0])
            rss_bad[power, error] = float(values[1])

xs = sorted(xs)
ys = sorted(ys)
X, Y = np.meshgrid(xs, ys)


fig, ax = plt.subplots(subplot_kw={"projection": "3d"})
surf = ax.plot_surface(X, Y, rss_good, cmap=cm.coolwarm,
                       linewidth=0, antialiased=False)
ax.set_xlabel("noise")
ax.set_ylabel("power")
ax.set_zlabel("rsum")

fig, ax = plt.subplots(subplot_kw={"projection": "3d"})
surf = ax.plot_surface(X, Y, rss_bad, cmap=cm.coolwarm,
                       linewidth=0, antialiased=False)
ax.set_xlabel("noise")
ax.set_ylabel("power")
ax.set_zlabel("rsum")

fig, ax = plt.subplots(subplot_kw={"projection": "3d"})
surf = ax.plot_surface(X, Y, success_rate, cmap=cm.coolwarm,
                       linewidth=0, antialiased=False)
ax.set_xlabel("noise")
ax.set_ylabel("power")
ax.set_zlabel("success")


X, Y = np.meshgrid(xs[1:], ys[1:])

rsum_ratios = np.divide(rsum_good[1:, 1:], rsum_bad[1:, 1:])
rss_ratios = np.divide(rss_good[1:, 1:], rss_bad[1:, 1:])
# for i in range(ratios.shape[0]):
#     for j in range(ratios.shape[1]):
#         if ratios[i,j] > 2:
#             ratios[i, j] = 2
#
fig, ax = plt.subplots(subplot_kw={"projection": "3d"})
surf = ax.plot_surface(X, Y, rss_ratios, cmap=cm.coolwarm,
                       linewidth=0, antialiased=False)
ax.set_xlabel("noise")
ax.set_ylabel("power")
ax.set_zlabel("ratios")
ax.set_title('Ratio between regular RSS and intruder RSS')
ax.set_zlim(0, 0.8)

fig, ax = plt.subplots(subplot_kw={"projection": "3d"})
surf = ax.plot_surface(X, Y, rsum_ratios, cmap=cm.coolwarm,
                       linewidth=0, antialiased=False)
ax.set_xlabel("noise")
ax.set_ylabel("power")
ax.set_zlabel("ratios")
ax.set_title('Ratio between regular RSUM and intruder RSUM')
ax.set_zlim(0, 0.8)

# fig, ax = plt.subplots()
#
# c = ax.pcolormesh(X, Y, rsum_bad, cmap='RdBu')
# set the limits of the plot to the limits of the data
# ax.axis([x.min(), x.max(), y.min(), y.max()])
# fig.colorbar(c, ax=ax)

plt.show()