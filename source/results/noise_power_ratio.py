import os
os.environ['PYGAME_HIDE_SUPPORT_PROMPT'] = "hide"
import sys
sys.path.append("/home/simon/Desktop/flocker/flocking-simulation/venv/lib/python3.9/site-packages/")
sys.path.append("/home/simon/Desktop/flocker/flocking-simulation/")
import source.world
import source.layouts
import source.olafi_flocking
import numpy as np
from source.watchdog import Watchdog

folder = 'data/noise_power_ratio/'



args = sys.argv[0]

t_max = 2000
good_count = 50
bad_count = 5
if len(sys.argv) > 1:
    p_error = float(sys.argv[1])/4
    v_error = float(sys.argv[2])/4
    attack_power = float(sys.argv[3])
    folder += sys.argv[4]
    if not os.path.isdir(folder):
        os.mkdir(folder)
else:
    p_error = 1
    v_error = 1
    attack_power = 5
    folder += 'run_1/'
    if not os.path.isdir(folder):
        os.mkdir(folder)

w = source.layouts.EmptyWorld(good_count, bad_count, attack_power)
w.change_noise(p_error, v_error)

watcher = Watchdog()

successes = np.zeros(t_max)

print("started " + str(sys.argv[1]) + ',' + str(sys.argv[3]))

# Actual simulation. At each timestep we save RSUM and RSS information
for t in range(t_max):
    if t % 100 == 0 and len(sys.argv) == 1:
        print(t)
    w.update(0.02)
    watcher.watch(w)

    successes[t] = watcher.success_rate

print("ended " + str(sys.argv[1]) + ',' + str(sys.argv[3]))

rsum_good = 0.0
rsum_bad = 0.0
rss_good = 0.0
rss_bad = 0.0
for b in w.birds:
    if type(b) == source.olafi_flocking.OlfatiIntruder:
        rsum_bad += b.rsum.length()
        rss_bad += b.rss
    else:
        rsum_good += b.rsum.length()
        rss_good += b.rss

rsum_good /= good_count
rsum_bad /= bad_count
rss_good /= good_count
rss_bad /= bad_count

folder += 'error_' + str(p_error) + '/'
if not os.path.isdir(folder):
    os.mkdir(folder)

folder += 'power_' + str(attack_power) + '/'

if not os.path.isdir(folder):
    os.mkdir(folder)

success_rate = sum(successes)/len(successes)

with open(folder+'data.dat', 'w') as f:
    f.write(str(rsum_good) + ',' + str(rsum_bad)+ ',' + str(success_rate) + '\n')
    f.write(str(rss_good) + ',' + str(rss_bad))



