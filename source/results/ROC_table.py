import source.layouts
import numpy as np
import matplotlib.pyplot as plt

good_count = 100
bad_count = 5

powers = np.linspace(2, 10, 5)
TPs = np.zeros(len(powers))
FPs = np.zeros(len(powers))

TPs_rss = np.zeros(len(powers))
FPs_rss = np.zeros(len(powers))

print(powers)

rep = 5
for r in range(rep):
    for p_i, p in enumerate(powers):

        w = source.layouts.SteadyWorld(good_count, bad_count, attack_power=p)
        w.change_noise(1, 1)

        for i in range(500):
            w.update(0.02)

        rsums = []
        rsss = []

        for b in w.birds:
            rsums.append(b.rsum.length())
            rsss.append(b.rss)

        median_rsum = np.median(rsums)
        median_rss = np.median(rsss)
        print(median_rsum, median_rss)


        c = 1.5
        tp = 0
        fp = 0
        tp_rss = 0
        fp_rss = 0
        for b in w.birds:
            if b.rsum.length() > c * median_rsum:
                if type(b) == source.olafi_flocking.OlfatiIntruder:
                    tp += 1
                else:
                    fp += 1

            if b.rss > c * median_rss:
                if type(b) == source.olafi_flocking.OlfatiIntruder:
                    tp_rss += 1
                else:
                    fp_rss += 1
        TPs[p_i] += tp
        FPs[p_i] += fp

        TPs_rss[p_i] += tp_rss
        FPs_rss[p_i] += fp_rss

# print(TPs)

TPs = np.divide(TPs, rep*bad_count)
FPs = np.divide(FPs, rep*good_count)
TPs_rss = np.divide(TPs_rss, rep*bad_count)
FPs_rss = np.divide(FPs_rss, rep*good_count)
print(powers)
print(TPs)
print(FPs)
print(TPs_rss)
print(FPs_rss)



plt.plot(FPs, TPs, marker="*", label='RSUM')
plt.plot(FPs_rss, TPs_rss, marker='o', label ='RSS')
plt.plot([0,1], [0,1], linestyle='--')
plt.xlim(0, 1)
plt.ylim(0, 1)
plt.xlabel('False Positive Rate (FP)')
plt.ylabel('True Positive Rate (TP)')
plt.legend()
plt.show()

print('done')