import source.layouts
import numpy as np
import matplotlib.pyplot as plt

good_count = 100
bad_count = 5

cs = np.linspace(1, 3, 20)
TPs = np.zeros(len(cs))
FPs = np.zeros(len(cs))

TPs_rss = np.zeros(len(cs))
FPs_rss = np.zeros(len(cs))


rep = 5
for r in range(rep):
    w = source.layouts.SteadyWorld(good_count, bad_count, attack_power=5)
    w.change_noise(1, 1)

    for i in range(500):
        w.update(0.02)

    rsums = []
    rsss = []

    for b in w.birds:
        rsums.append(b.rsum.length())
        rsss.append(b.rss)

    median_rsum = np.median(rsums)
    median_rss = np.median(rsss)
    print(median_rsum, median_rss)



    for i, c in enumerate(cs):
        tp = 0
        fp = 0
        tp_rss = 0
        fp_rss = 0
        for b in w.birds:
            if b.rsum.length() > c * median_rsum:
                if type(b) == source.olafi_flocking.OlfatiIntruder:
                    tp += 1
                else:
                    fp += 1

            if b.rss > c * median_rss:
                if type(b) == source.olafi_flocking.OlfatiIntruder:
                    tp_rss += 1
                else:
                    fp_rss += 1
        TPs[i] += tp
        FPs[i] += fp

        TPs_rss[i] += tp_rss
        FPs_rss[i] += fp_rss

print(TPs)

TPs = np.divide(TPs, rep*bad_count)
FPs = np.divide(FPs, rep*good_count)
TPs_rss = np.divide(TPs_rss, rep*bad_count)
FPs_rss = np.divide(FPs_rss, rep*good_count)

plt.plot(FPs, TPs, marker="*", label='RSUM')
plt.plot(FPs_rss, TPs_rss, marker='o', label ='RSS')
plt.plot([0,1], [0,1], linestyle='--')
plt.xlim(0, 1)
plt.ylim(0, 1)
plt.xlabel('False Positive Rate (FP)')
plt.ylabel('True Positive Rate (TP)')
plt.legend()
plt.show()

print('done')