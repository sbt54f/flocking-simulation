import source.world
import source.layouts
import source.olafi_flocking
import numpy as np
from source.watchdog import Watchdog
import os

folder = 'data/over_time/'

# Create a new fulder for every new run, to save old data
if len(os.listdir(folder)) == 0:
    folder += 'run_1/'
else:
    run_count = int(sorted(os.listdir(folder))[-1].split('_')[1])

    folder += 'run_' + str(run_count+1) + '/'
    os.mkdir(folder)



t_max = 10000

good_count = 50
bad_count = 5
p_error = 1
v_error = 1
attack_power = 5

# Save settings for reference
with open(folder + 'settings.dat', 'w') as f:
    f.write(str(good_count) + ',' + str(bad_count) + ',' + str(p_error) + ',' + str(v_error) + ',' + str(attack_power) )

w = source.layouts.SteadyWorld(good_count, bad_count, attack_power)
w.change_noise(p_error, v_error)

watcher = Watchdog()

times = np.zeros(t_max)
good_rsums = np.zeros((t_max, good_count))
bad_rsums = np.zeros((t_max, bad_count))

good_rss = np.zeros((t_max, good_count))
bad_rss = np.zeros((t_max, bad_count))



success_time = -1
successes = np.zeros(t_max)


# Actual simulation. At each timestep we save RSUM and RSS information
for t in range(t_max):
    if t%100 == 0:
        print(t)
    w.update(0.02)
    watcher.watch(w)
    for b in w.birds:
        if type(b) == source.olafi_flocking.OlfatiIntruder:
            bad_rsums[t, b.id-good_count] = b.rsum.length()
            bad_rss[t, b.id-good_count] = b.rss
        else:
            good_rsums[t][b.id] = b.rsum.length()
            good_rss[t][b.id] = b.rss
    times[t] = t
    successes[t] = 0 if not watcher.successful_attack else 1

with open(folder+'good_rsum.dat', 'w') as f:
    for i in range(t_max):
        f.write(str(times[i]))
        for j in range(good_count):
            f.write(', ' + str(good_rsums[i, j]))
        f.write('\n')


with open(folder+'bad_rsum.dat', 'w') as f:
    for i in range(t_max):
        f.write(str(times[i]))
        for j in range(bad_count):
            f.write(', ' + str(bad_rsums[i, j]))
        f.write('\n')

with open(folder+'good_rss.dat', 'w') as f:
    for i in range(t_max):
        f.write(str(times[i]))
        for j in range(good_count):
            f.write(', ' + str(good_rss[i, j]))
        f.write('\n')


with open(folder+'bad_rss.dat', 'w') as f:
    for i in range(t_max):
        f.write(str(times[i]))
        for j in range(bad_count):
            f.write(', ' + str(bad_rss[i, j]))
        f.write('\n')

with open(folder+'success.dat', 'w') as f:
    for i in range(len(bad_rsums)):
        f.write(str(successes[i]) + '\n')
