import math
import os
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm

folder = 'data/success_heat/'

run_count = int(sorted(os.listdir(folder))[-1].split('_')[1])

folder += 'run_' + str(7) + '/'



errors = os.listdir(folder)
errors = sorted([float(f.split('_')[1]) for f in errors])


powers = os.listdir(folder + os.listdir(folder)[0])
powers = sorted([float(p.split('_')[1]) for p in powers])

times = np.zeros((len(errors), len(powers)))
rsum_max = np.zeros((len(errors), len(powers)))
rsum_bad = np.zeros((len(errors), len(powers)))
rss_max = np.zeros((len(errors), len(powers)))
rss_bad = np.zeros((len(errors), len(powers)))

i = 0
for error_file in sorted(os.listdir(folder)):
    error = float(error_file.split('_')[1])

    for power_file in sorted(os.listdir(folder + error_file)):
        power = float(power_file.split('_')[1])

        with open(folder + error_file + '/' + power_file + '/data.dat', 'r') as f:
            values = [float(v) for v in f.readline().split(',')]
            # time = float(f.readline().split(',')[2])
            # if time > 2000:
            #     time = 2000

            times[powers.index(power), errors.index(error)] = values[0]
            rsum_bad[powers.index(power), errors.index(error)] = min(values[2], 20)
            rsum_max[powers.index(power), errors.index(error)] = min(values[3], 20)
            rss_bad[powers.index(power), errors.index(error)] = values[5]
            rss_max[powers.index(power), errors.index(error)] = values[6]

X, Y = np.meshgrid(errors, powers)


fig, ax = plt.subplots(subplot_kw={"projection": "3d"})
surf = ax.plot_surface(X, Y, times, cmap=cm.coolwarm,
                       linewidth=0, antialiased=False)
ax.set_xlabel("noise")
ax.set_ylabel("power")
ax.set_zlabel("time")
ax.set_title('time')

fig.colorbar(surf, ax=ax)

fig, ax = plt.subplots(subplot_kw={"projection": "3d"})
surf = ax.plot_surface(X, Y, rsum_bad, cmap=cm.coolwarm,
                       linewidth=0, antialiased=False)
ax.set_xlabel("noise")
ax.set_ylabel("power")
ax.set_zlabel("rsum_bad")
ax.set_title('rsum bad')


fig.colorbar(surf, ax=ax)

fig, ax = plt.subplots(subplot_kw={"projection": "3d"})
surf = ax.plot_surface(X, Y, rsum_max, cmap=cm.coolwarm,
                       linewidth=0, antialiased=False)
ax.set_xlabel("noise")
ax.set_ylabel("power")
ax.set_zlabel("maxes")
ax.set_title('rsum max')


fig.colorbar(surf, ax=ax)

fig, ax = plt.subplots(subplot_kw={"projection": "3d"})
surf = ax.plot_surface(X, Y, rss_max, cmap=cm.coolwarm,
                       linewidth=0, antialiased=False)
ax.set_xlabel("noise")
ax.set_ylabel("power")
ax.set_zlabel("rss max")
ax.set_title('rss max')


fig.colorbar(surf, ax=ax)

fig, ax = plt.subplots(subplot_kw={"projection": "3d"})
surf = ax.plot_surface(X, Y, rss_bad, cmap=cm.coolwarm,
                       linewidth=0, antialiased=False)
ax.set_xlabel("noise")
ax.set_ylabel("power")
ax.set_zlabel("rss bad")
ax.set_title('rss bad')


fig.colorbar(surf, ax=ax)


rsum_ratio = np.divide(rsum_bad, rsum_max)
fig, ax = plt.subplots(subplot_kw={"projection": "3d"})
surf = ax.plot_surface(X, Y, rsum_ratio, cmap=cm.coolwarm,
                       linewidth=0, antialiased=False)
ax.set_xlabel("noise")
ax.set_ylabel("power")
ax.set_zlabel("rsum ratio")
ax.set_title('rsum ratio')


fig.colorbar(surf, ax=ax)

plt.show()