import pygame

class Config:
    def __init__(self):
        self.draw_groups = False
        self.pause = True
        self.iteration_count = 0
        self.camera = pygame.Vector2(0, 0)
        self.camera_width = 1200
        self.camera_height = 800
        self.camera_offset = pygame.Vector2(0, 0)
        self.camera_zoom = 1.0
        self.avg_velocity = pygame.Vector2(0.0, 0.0)
        self.compass_pos = pygame.Vector2(50, 50)
        self.compass_radius = 50
        self.p_noise_slider = pygame.Rect(5, 110, 20, 200)
        self.v_noise_slider = pygame.Rect(35, 110, 20, 200)
        self.p_noise_level = 0.00
        self.v_noise_level = 0.00
        self.target = pygame.Vector2(0.0, 0.0)
        self.shepherd_target = pygame.Vector2(0.0, 0.0)
        self.attack_power = 0

        self.intruders = []
