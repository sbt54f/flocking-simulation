import pygame
import random


def weighted_target_rule(weight=1):
    return lambda w, bird : _get_current_target(w, bird, weight)

def _get_current_target(w, bird, weight):
    if len(bird.target_sequence) > 0:
        target = bird.target_sequence[bird.current_target] - bird.p
        if target.length_squared() < w.target_range ** 2:
            # reached target, need new target
            bird.current_target += 1
            bird.time_since_target = 0
            if bird.current_target == len(bird.target_sequence):
                bird.current_target = 0
        target = bird.target_sequence[bird.current_target] - bird.p

        if target.length_squared() > 0:
            target = target.normalize()
    else:
        target = pygame.Vector2(0, 0)

    return target * weight

def weighted_direction_rule(weight=1):
    return lambda w, bird : _get_direction(w, bird, weight)

def _get_direction(w, bird, weight):

    target = w.target
    return target * weight

def weighted_avoidance_rule(weight, avoid_range):
    return lambda w, bird : _calculate_avoidance(bird.p_measurements, weight, avoid_range)


def _calculate_avoidance(ps, weight, avoid_range):
    avoidance = pygame.math.Vector2(0, 0)
    for distance in ps:
        length = distance.length()
        if length == 0:
            # This case helps break birds apart when stuck in the same position
            avoidance += pygame.Vector2((random.randint(0, 1) - 0.5) * 0.1, (random.randint(0, 1) - 0.5) * 0.1)
        elif length < avoid_range:
            avoidance += -distance * (avoid_range-length)

    return avoidance * weight

def weighted_alignment_rule(weight):
    return lambda w, bird : _calculate_alignment(bird.v_measurements, weight)

def _calculate_alignment(vs, weight):
    alignment = pygame.math.Vector2(0, 0)
    for v in vs:
        alignment += v

    alignment = alignment/len(vs)
    return alignment * weight


def weighted_attraction_rule(weight):
    return lambda w, bird : _calculate_attraction(bird.p_measurements, weight)

def _calculate_attraction(ps, weight):
    attraction = pygame.math.Vector2(0, 0)

    for distance in ps:
        attraction += distance

    attraction /= len(ps)

    return attraction * weight

