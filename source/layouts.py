from source.world import World
from source.bird import Bird
from source.intruder import NonFlocker
from source.rules import *
from source.olafi_flocking import *
import pygame
import random
import math
import numpy as np

class Formation(World):
    def __init__(self, width, height, degree, row_count=10, column_count=20):
        super().__init__(width, height)
        Bird.neighborhood_size = degree
        predicted_d = 100-(0.2 / (0.02 * degree))
        # predicted_d = 100
        h_margin = 0
        count = 0
        start_x = 200
        if degree == 6:
            move_x = predicted_d/2
            height = math.sqrt(predicted_d**2-move_x**2)
        elif degree == 4:
            move_x = 0
            height = predicted_d
        elif degree == 3:
            move_x = predicted_d / 2
            height = math.sqrt(predicted_d ** 2 - move_x ** 2)
        for row in range(0, row_count):
            h_margin += height
            start_x += move_x
            move_x *= -1
            for column in range(0, column_count):
                if row == 0 or row == row_count-1 or column == 0 or column == column_count-1:
                    # self.addBird(Dummy(start_x + (predicted_d*column), 100 + h_margin, count))
                    pass
                if column == int(column_count/2) and row == int(row_count/2) :
                    # self.addBird(Shepherd(start_x + (predicted_d * column), 100 + h_margin, count))
                    pass
                else:
                    b = create_direction_bird(start_x + (predicted_d * column), 100 + h_margin, count, 1, 1)

                    # b = Bird(start_x + (predicted_d * column), 100 + h_margin, count)
                    self.addBird(b)
                    b.v = (self.target.normalize() * (Bird.max_speed))
            # if Bird.neighborhood_size == 4 and (i == 0 or i == 9 or i == 90 or i == 99):
            #     continue


            count += 1
        self.p_stds = np.full(len(self.birds), 0.0)
        self.v_stds = np.full(len(self.birds), 0.0)

class EmptyWorld(World):
    def __init__(self, n, intruders=0, power=10):
        super().__init__()
        # predicted_d = Bird.avoid_range-(Bird.attraction_weight / (Bird.avoidance_weight * Bird.neighborhood_size))
        # predicted_d = 150
        h_margin = 0
        count = 0

        for i in range(0, n):

            self.birds.append(OlfatiBird(random.gauss(0, 20), random.gauss(0, 20), i))
            count += 1

        for i in range(n, n+intruders):
            x = -(6.06 / 2)
            y = (int((i-n + 1) / 2) * 3.5)
            if ((len(self.birds)) % 4) in [1, 2]:
                x = 6.06 / 2
            if len(self.birds) % 2 == 0:
                y *= -1
            target = pygame.Vector2(x, y)
            new_intruder = OlfatiIntruder(random.gauss(0, 5), random.gauss(0, 5)+400, i, target, power)
            # c.intruders.append(new_intruder)
            self.birds.append(new_intruder)
            count += 1

        self.p_stds = np.full(len(self.birds), 0.0)
        self.v_stds = np.full(len(self.birds), 0.0)

class SteadyWorld(World):
    def __init__(self, n, intruders=0, attack_power=1):
        super().__init__()
        count = 0
        with open('yoyo.dat', 'r') as f:
            gamma = f.readline().split(',')
            self.gamma_agent.p.x = float(gamma[0])
            self.gamma_agent.p.y = float(gamma[1])

            while count < n:
                bird_text = f.readline().split(',')
                bird = OlfatiBird(float(bird_text[0]), float(bird_text[1]), count)
                bird.v = pygame.Vector2(float(bird_text[2]), float(bird_text[3]))
                bird.a = pygame.Vector2(float(bird_text[4]), float(bird_text[5]))
                self.birds.append(bird)
                count += 1

            while count < n+intruders:
                x = -(6.06 / 2)
                y = (int((count - n + 1) / 2) * 3.5)
                if ((len(self.birds)) % 4) in [1, 2]:
                    x = 6.06 / 2
                if len(self.birds) % 2 == 0:
                    y *= -1
                bird_text = f.readline().split(',')
                target = pygame.Vector2(x, y)
                bird = OlfatiIntruder(float(bird_text[0]), float(bird_text[1]), count, target, power=attack_power)
                bird.v = pygame.Vector2(float(bird_text[2]), float(bird_text[3]))
                bird.a = pygame.Vector2(float(bird_text[4]), float(bird_text[5]))
                self.birds.append(bird)
                count += 1



        self.p_stds = np.full(len(self.birds), 0.0)
        self.v_stds = np.full(len(self.birds), 0.0)


class HourGlass(World):
    def __init__(self, width, height, good_count, bad_count, p_std=1, v_std=1, intruder_type=NonFlocker, y_max_distance=2000):
        super().__init__(width, height)
        targets = [[200, 100], [600, 275], [1000, 100],
                   [1000, 600], [600, 425], [200, 600]]
        for t in targets:
            self.targets.append(pygame.Vector2(t[0], t[1]))

        good_y_distance = y_max_distance/good_count

        for i in range(0, good_count):
            # bird = Bird(200 + ((i%2))*40, 300 + (good_y_distance * i), i, p_std=p_std, v_std=v_std)
            # bird.target_sequence = self.targets
            # bird.flocking_rules.append(weighted_target_rule(10))
            # bird.flocking_rules.append(weighted_avoidance_rule(0.02, 100))
            # bird.flocking_rules.append(weighted_alignment_rule(0.4))
            # bird.flocking_rules.append(weighted_attraction_rule(0.2))

            self.birds.append(create_target_bird(200 + ((i%2))*40, 300 + (good_y_distance * i), i, p_std, v_std, self.targets))

        if bad_count > 0:
            bad_y_distance = (y_max_distance-300)/bad_count
            for i in range(0, bad_count):
                bird = intruder_type(200 + (i % 2) * 40, 700 + (bad_y_distance * i), i + good_count, p_std=p_std, v_std=v_std)
                bird.target_sequence = self.targets
                self.birds.append(bird)

        self.p_stds = np.full(good_count + bad_count, p_std)
        self.v_stds = np.full(good_count + bad_count, v_std)


def create_target_bird(x, y, i, p_std, v_std, targets):
    bird = Bird(x, y, i, p_std=p_std, v_std=v_std)
    bird.target_sequence = targets
    bird.flocking_rules.append(weighted_target_rule(10))
    bird.flocking_rules.append(weighted_avoidance_rule(0.002, 100))
    bird.flocking_rules.append(weighted_alignment_rule(0.4))
    bird.flocking_rules.append(weighted_attraction_rule(0.2))
    return bird

def create_direction_bird(x, y, i, p_std, v_std):
    bird = Bird(x, y, i, p_std=p_std, v_std=v_std)
    bird.flocking_rules.append(weighted_direction_rule(10))
    bird.flocking_rules.append(weighted_avoidance_rule(0.02, 100))
    bird.flocking_rules.append(weighted_alignment_rule(0.4))
    bird.flocking_rules.append(weighted_attraction_rule(0.2))
    return bird


class Merge(World):
    def __init__(self, width, height, good_count, bad_count, p_std=1, v_std=1, intruder_type=NonFlocker):
        super().__init__(width, height)
        targets = [[200, 350], [600, 350], [1000, 350], [10000, 350]]
        for t in targets:
            self.targets.append(pygame.Vector2(t[0], t[1]))

        for i in range(0, int(good_count/2)):
            bird = Bird(((i * -15) + (i % 2) * 40), 0 + (-15 * i) + (i % 2) * 12, i, p_std, v_std)
            bird.target_sequence = self.targets
            self.birds.append(bird)

        for i in range(int(good_count/2), good_count):
            bird = Bird((((i-(good_count/2))*-15)+((i%2))*40), 600 + (15 * (i-(good_count/2)))+((i%2))*12, i, p_std, v_std)
            bird.target_sequence = self.targets
            self.birds.append(bird)

        n = good_count

        for i in range(0, int(bad_count/2)):
            bird = intruder_type((((i * 5) * -17) - 71), -50 + (-40 * i) + ((i % 2)) * 12, n, p_std, v_std)
            bird.target_sequence = self.targets
            self.birds.append(bird)
            n += 1

        for i in range(0, int(bad_count/2)):
            bird = intruder_type((((i * 5) * -17) - 91), 650 + (40 * (i)) + ((i % 2)) * 12, n, p_std, v_std)
            bird.target_sequence = self.targets

            self.birds.append(bird)
            n += 1

        self.p_stds = np.full(good_count + bad_count, p_std)
        self.v_stds = np.full(good_count + bad_count, v_std)


class Circle(World):
    def __init__(self, width, height, good_count, bad_count, p_std=1, v_std=1, intruder_type=NonFlocker, radius=400, target_count=5):
        super().__init__(width, height)
        for i in range(0, good_count):
            self.birds.append(Bird(200 + ((i%2))*40, 500 + (15 * i), i, p_std=1, v_std=1))

        for i in range(0, bad_count):
            self.birds.append(intruder_type(200 + ((i % 2)) * 40, 550 + (65 * i), i + good_count, p_std=1, v_std=1))

        if target_count == 1:
            self.targets.append(pygame.Vector2(width/2, height/2))
        else:
            initial_x = width/2
            initial_y = (height/2)
            step_length = (2*math.pi)/target_count
            for i in range(0, target_count):
                x = initial_x - (math.cos(i*step_length) * radius)
                y = initial_y - (math.sin(i*step_length) * radius)
                self.targets.append(pygame.Vector2(x, y))

        for bird in self.birds:
            bird.target_sequence = self.targets

        self.p_stds = np.full(good_count + bad_count, p_std)
        self.v_stds = np.full(good_count + bad_count, v_std)

class DualCircle(World):
    def __init__(self, width, height, good_count, bad_count):
        super().__init__(width, height)

        targets = [[200, 100], [600, 250], [200, 600],
                   [600, 450], [1000, 100], [1000, 600]]
        for t in targets:
            self.targets.append(pygame.Vector2(t[0], t[1]))
        left_targets = [self.targets[3], self.targets[1], self.targets[0], self.targets[2]]
        right_targets =[self.targets[3], self.targets[1], self.targets[4], self.targets[5]]

        for i in range(0, good_count):
            bird = Bird(600 + (50 * (i%2)), 600 + (25 * i), i)
            if i%2 == 0:
                bird.target_sequence.extend(left_targets)
            else:
                bird.target_sequence.extend(right_targets)
            self.birds.append(bird)


        for i in range(0, bad_count):
            bird = NonFlocker(200 + ((i % 2)) * 40, 350 + (65 * i), i+good_count)
            if i%2 == 0:
                bird.target_sequence.extend(left_targets)
            else:
                bird.target_sequence.extend(right_targets)
            self.birds.append(bird)


