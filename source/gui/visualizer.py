import math

import pygame
from source.bird import Bird
from source.world import World
import random
from source.olafi_flocking import OlfatiIntruder, AttackMode

mode_colors = {AttackMode.FIND: 'green', AttackMode.INFILTRATE: 'red', AttackMode.RESET: 'orange',
               AttackMode.SPLIT: 'red'}


class Visualizer:
    def __init__(self, width, height, data_height = 200):
        pygame.init()
        self.surface = pygame.display.set_mode((width, height + data_height))

        self.bird_width = bird_width = 40
        self.fixed_wing_img = pygame.transform.scale(pygame.image.load('source/gui/images/fixed_wing.png'), (bird_width, bird_width))
        self.bad_img = pygame.transform.scale(pygame.image.load('source/gui/images/bad.png'), (bird_width, bird_width))
        self.dummy_img = pygame.transform.scale(pygame.image.load('source/gui/images/dummy.png'), (bird_width, bird_width))

        self.target_img = pygame.transform.scale(pygame.image.load('source/gui/images/flag.png'), (20, 20))

        self.arrow_img = pygame.transform.scale(pygame.image.load('source/gui/images/arrow.png'), (40, 40))
        self.evil_img = pygame.transform.scale(pygame.image.load('source/gui/images/evil.png'), (40, 40))

        self.transparent_surface = pygame.Surface([width, height], pygame.SRCALPHA, 32)
        self.transparent_surface = self.transparent_surface.convert_alpha()

        self.delta_font = pygame.font.SysFont(None, 72)

        self.noise_font = pygame.font.SysFont(None, 20)

        self.attackModes = ['Find', 'Infiltrate', 'Reset', 'Split']

    def draw_groups(self, screen, world, config):

        for bird in world.birds:
            # if type(bird) != Bird:
            #     print('group')
            #     continue
            for n in bird.neighbours:
                neighbor = world.birds[n]

                distance = (neighbor.p - bird.p)
                b_p = pygame.Vector2(bird.p.x, bird.p.y)
                # if int(bird.p.x /width) == int(neighbor.p.x /width) and int(bird.p.y / height) == int(neighbor.p.y / height):
                n_p = b_p + distance
                # else:
                #     n_p = neighbor.p


                edge_color = 0
                pygame.draw.line(screen, (edge_color, edge_color, edge_color), (b_p - config.camera)*config.camera_zoom, (n_p - config.camera)*config.camera_zoom,
                                 int(1))

    def draw_target(self, surface, t, config):
        pos = ((t[0] - config.camera.x)*config.camera_zoom, (t[1] - config.camera.y)*config.camera_zoom)
        surface.blit(self.target_img, (pos[0] - 10, pos[1] - 10))
        pygame.draw.circle(surface, (0, 0, 0), pos, int(10)*config.camera_zoom, 1)

    # draws a background to show the movement of birds
    def draw_background(self, surface, config):
        linewidth = 200*config.camera_zoom
        lineheight = 200*config.camera_zoom

        start_pos = pygame.Vector2(0, -(config.camera.y*config.camera_zoom)%lineheight)
        start_pos.x = 0
        end_pos = pygame.Vector2(0, -(config.camera.y*config.camera_zoom)%lineheight)
        end_pos.x = surface.get_width()

        for i in range(0, int(surface.get_height()/lineheight)+1):
            pygame.draw.line(surface, (0, 0, 0), start_pos, end_pos, 1)

            start_pos.y += lineheight
            end_pos.y += lineheight

            if start_pos.y > surface.get_height():
                new_y = (-config.camera.y)*config.camera_zoom - lineheight
                start_pos.y = new_y
                end_pos.y = new_y
                lineheight *= -1

        start_pos.x = -(config.camera.x*config.camera_zoom)%linewidth
        start_pos.y = 0
        end_pos.x = -(config.camera.x*config.camera_zoom)%linewidth
        end_pos.y = surface.get_height()
        #
        # # start_pos.y = ((start_pos.y - surface.get_height())% (surface.get_height()*2))
        # # end_pos.y = start_pos.y + surface.get_height()*2
        for i in range(0, int(surface.get_width()/linewidth)+1):
            pygame.draw.line(surface, (0, 0, 0), start_pos, end_pos, 1)

            start_pos.x += linewidth
            end_pos.x += linewidth

            if start_pos.x > surface.get_width():
                new_x = (-config.camera.x)*config.camera_zoom - linewidth
                start_pos.x = new_x
                end_pos.x = new_x
                linewidth *= -1

    # this function outlines the task of drawing
    def draw(self, w, dt, config, chart):
        # fill the screen with white
        self.surface.fill((255, 255, 255))
        self.transparent_surface.fill((255, 255, 255, 0))


        if len(w.birds) > 0:
            config.avg_velocity = (w.v_sum / len(w.birds)) / Bird.max_speed

        # print((config.flock_center-w.gamma_agent.p).length())

        config.camera = w.flock_center - config.camera_offset
        config.camera.x -= config.camera_width/(config.camera_zoom*2)
        config.camera.y -= config.camera_height/(config.camera_zoom*2)


        self.draw_background(self.surface, config)

        delta_text = self.delta_font.render(str(dt), True, (0, 0, 0))
        self.surface.blit(delta_text, (10, config.camera_height - 50))

        if config.draw_groups:
            self.draw_groups(self.surface, w, config)

        # draw each bird
        for bird in w.birds:
            self.draw_bird(self.surface, bird, config)
            if bird.intruder:
                target = pygame.Vector2(w.gamma_agent.p.x, w.flock_center.y)
                target += bird.formation_target
                # if (bird.id % 2) == 0:
                #     target.x += 2
                target = (target-config.camera)*config.camera_zoom
                pygame.draw.circle(self.surface, 'red', target, 2)

        # for t in w.targets:
        #     self.draw_target(self.surface, t, config)

        self.draw_target(self.surface, w.gamma_agent.p, config)

        self.drawCompass(self.surface, config)

        self.drawNoiseSlider(self.surface, config.p_noise_slider, config.p_noise_level)
        self.drawNoiseSlider(self.surface, config.v_noise_slider, config.v_noise_level)

        self.drawAttackMode(self.surface, config)

        center = (w.flock_center-config.camera)*config.camera_zoom
        pygame.draw.circle(self.surface, 'grey', center, 2*config.camera_zoom, 1)
        # if chart:
        #     self.charter.draw(self.data_screen)

        # self.drawTargetZone(self.transparent_surface, config, w)

        self.surface.blit(self.transparent_surface, (0, 0))

        # finished drawing
        pygame.display.flip()

    def draw_bird(self, surface, bird, config):
        img = self.fixed_wing_img
        color = 'black'
        if type(bird) == OlfatiIntruder:
            color = mode_colors[bird.mode]
            img = self.evil_img
        # img = pygame.transform.scale(img, (int(self.bird_width * (config.camera_zoom/20)), int(self.bird_width * (config.camera_zoom/20))))
        # if not type(bird) == Bird:
        #     img = bad_img
        x = int(((bird.p.x - config.camera.x)* config.camera_zoom  )  )
        y = int(((bird.p.y - config.camera.y)* config.camera_zoom  ) )

        l = bird.rsum.length()
        if l > 0:
            scaled_rsum = bird.rsum.normalize()*math.sqrt(bird.rsum.length())
        else:
            scaled_rsum = pygame.Vector2(0,0)
        rsum_x = x+scaled_rsum.x*config.camera_zoom
        rsum_y = y+ scaled_rsum.y*config.camera_zoom
        if bird.marked:
            pygame.draw.circle(surface, (255, 0, 0), (x, y), 20, 1)

        # surface.blit(pygame.transform.rotate(img, bird.v.angle_to((0, -1))), (int(bird.p.x%width) - 10, int(bird.p.y%height) - 10))

        # elif type(bird) != Bird or bird.intruder:
        # pygame.draw.circle(surface, color, (x, y), 3.5*config.camera_zoom, 1)
        pygame.draw.line(surface, (255*bird.intruder,0,0), (x, y), (rsum_x, rsum_y))


        # pygame.transform.scale(pygame.image.load('source/gui/images/flag.png'), (20, 20))

        # if img.get_width() == 0:
        #     print('whoops, too small img')
        # else:
        surface.blit(pygame.transform.rotate(img, bird.v.angle_to((0, -1))), (x- img.get_width()/ 2, y- img.get_width() / 2))
        # circle_radius = bird.attraction_factor.length()
        # pygame.draw.circle(transparent_surface, (150, 0, 0, 128), (int(bird.p.x - config.camera.x), int(bird.p.y - config.camera.y)), circle_radius, 0)

    def drawAttackMode(self, surface, config):
        modeCount = [0]*len(self.attackModes)
        for bad in config.intruders:
            modeCount[bad.mode.value] += 1
        x = config.camera_width-60
        for mode in range(0, len(self.attackModes)):
            text = self.noise_font.render(self.attackModes[mode], True, (0, 0, 0))
            y = 10+(mode*16)
            surface.blit(text, (x, y))
            if modeCount[mode] > 0:
                pygame.draw.circle(surface, (0,0,0), (x-5, y+8), 2)

    def drawCompass(self, surface, config):
        radius = config.compass_radius
        center = config.compass_pos
        vel = center + (config.avg_velocity * radius)
        target = center + (config.target / Bird.max_speed * radius)
        shepherd_target = center + (config.shepherd_target / Bird.max_speed * radius)
        pygame.draw.circle(surface, (0, 0, 0), center, radius, 2)
        pygame.draw.line(surface, (0, 0, 0), center, vel, 2)
        pygame.draw.line(surface, (0, 0, 0), center, target, 1)
        pygame.draw.line(surface, (255, 0, 0), center, shepherd_target, 1)

    def drawTargetZone(self, s, config, w):
        bird = w.birds[0]

        triangles = Visualizer.find_triangles(w)

        for triangle in triangles:
            targets = ((w.birds[triangle[0]].p - config.camera)*config.camera_zoom, (w.birds[triangle[1]].p - config.camera)*config.camera_zoom,
                       (w.birds[triangle[2]].p - config.camera)*config.camera_zoom)
            random.seed(triangle[0] * triangle[1] * triangle[2])
            pygame.draw.polygon(s, (random.randint(0, 255), random.randint(0, 255), random.randint(0, 255), 128),
                                targets)

    def find_triangles(w):
        results = set()

        counts = [0] * len(w.birds)
        for current in range(0, len(w.birds)):

            for n in w.birds[current].neighbours:
                for nn in w.birds[n].neighbours:
                    if nn in w.birds[current].neighbours:
                        # searchset.add(n)
                        results.add(tuple(sorted((current, n, nn))))
                        counts[current] += 1

        for i in range(0, len(counts)):
            if counts[i] / 2 >= 8:
                w.birds[i].marked = True
        return results

    def drawNoiseSlider(self, surface, slider, level):
        pygame.draw.rect(surface, (0, 0,0), slider, 1)
        pygame.draw.line(surface, (0,0,0), (slider.x, slider.y+slider.height - (level*slider.height)), (slider.x+slider.width, slider.y+slider.height - (level*slider.height)))
        noise_text = self.noise_font.render(str(level), True, (0, 0, 0))
        self.surface.blit(noise_text, (slider.x, slider.y+slider.height + 5))
