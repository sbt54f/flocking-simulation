import pygame
from source.bird import Bird
from source import intruder
from source.olafi_flocking import *
from pygame.locals import (
    K_s,
    K_a,
    K_i,
    K_r,
    K_UP,
    K_DOWN,
    K_LEFT,
    K_RIGHT,
    KEYDOWN,
    K_SPACE,
    MOUSEBUTTONDOWN,
    K_EQUALS,
    K_MINUS
)



def handle_inputs(config):
    pressed = pygame.key.get_pressed()

    if pygame.mouse.get_pressed()[0]:
        compass_dir = (pygame.mouse.get_pos() - config.compass_pos)
        if compass_dir.length() < config.compass_radius:
            t = (compass_dir / config.compass_radius) * Bird.max_speed
            if pressed[pygame.K_LSHIFT]:
                config.shepherd_target = t
            else:
                config.target = t



    camera_speed = 10
    if pressed[K_UP]:
        config.camera_offset.y += camera_speed/config.camera_zoom
    if pressed[K_DOWN]:
        config.camera_offset.y -= camera_speed/config.camera_zoom
    if pressed[K_LEFT]:
        config.camera_offset.x += camera_speed/config.camera_zoom
    if pressed[K_RIGHT]:
        config.camera_offset.x -= camera_speed/config.camera_zoom


def handle_event(e, w, c):
    m_pos = pygame.mouse.get_pos()
    w_pos = c.camera + pygame.Vector2(m_pos)/c.camera_zoom
    if e.type == MOUSEBUTTONDOWN:
        if e.button == 1:
            compass_dir = (m_pos - c.compass_pos)
            if c.p_noise_slider.collidepoint(m_pos[0], m_pos[1]):
                c.p_noise_level = ((c.p_noise_slider.y+c.p_noise_slider.height) - m_pos[1])/200
                w.change_noise(c.p_noise_level, c.v_noise_level)
            elif c.v_noise_slider.collidepoint(m_pos[0], m_pos[1]):
                c.v_noise_level = ((c.v_noise_slider.y+c.v_noise_slider.height) - m_pos[1])/200
                w.change_noise(c.p_noise_level, c.v_noise_level*20)

            elif compass_dir.length() > c.compass_radius:
                x = -(6.06/2)
                y = (int((OlfatiIntruder.count+1)/2)*3.5)
                if ((len(w.birds)) % 4) in [1, 2]:
                    x = 6.06/2
                if len(w.birds)%2 == 0:
                    y *= -1
                target = pygame.Vector2(x, y)
                new_intruder = OlfatiIntruder(w_pos[0], w_pos[1], 100, target)
                new_intruder.attack_power = c.attack_power
                c.intruders.append(new_intruder)
                w.addBird(new_intruder)
        elif e.button == 4:
            c.camera_zoom *= 1.2
        elif e.button == 5:
            c.camera_zoom *= 0.8
    elif e.type == KEYDOWN:
        if e.key == K_s:
            c.draw_groups = not c.draw_groups
        elif e.key == K_SPACE:
            c.pause = not c.pause
            if c.pause:
                print(c.iteration_count, c.watchdog.connected)
                save_birds_to_file(w, 'yoyo.dat')


        elif e.key == K_i:
            for b in c.intruders:
                b.mode = AttackMode.INFILTRATE
        elif e.key == K_EQUALS:
            for b in c.intruders:
                if ((b.id) % 4) in [1, 2]:
                    b.formation_target.x += 1
                else:
                    b.formation_target.x -= 1
        elif e.key == K_MINUS:
            for b in c.intruders:
                if ((b.id) % 4) in [1, 2]:
                    b.formation_target.x -= 1
                else:
                    b.formation_target.x += 1
        elif e.key == K_r:
            for b in c.intruders:
                if b.mode != AttackMode.FIND:
                    b.mode = AttackMode.RESET

def save_birds_to_file(w, file):
    with open(file, 'w') as f:
        f.write(str(w.gamma_agent.p.x) + ',' + str(w.gamma_agent.p.y) + '\n')
        for b in w.birds:
            f.write(str(b.p.x)+',' + str(b.p.y)+',' + str(b.v.x)+',' + str(b.v.y)+',' + str(b.a.x)+',' + str(b.a.y) + '\n')