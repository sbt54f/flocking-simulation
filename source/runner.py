import sys

import pygame
from sim_config import Config
from source import layouts
from source.bird import Bird
import intruder
from olafi_flocking import *
import numpy as np
from watchdog import Watchdog
from source.gui.visualizer import Visualizer
from source.gui.controller import handle_event, handle_inputs

import os
import random

random.seed('asdasdas')

os.chdir('../')


width = 1600
height = 1200
data_height = 0

good_count = 100
bad_count = 5
bird_count = good_count + bad_count


noise = 0.3
attack_power = 8
w = layouts.EmptyWorld(good_count, bad_count, power=attack_power)
# w = layouts.SteadyWorld(good_count, bad_count, attack_power=4)
# w = layouts.EmptyWorld(good_count, bad_count, 10)

# w.birds[50].p = pygame.Vector2(0, 200)

watchdog = Watchdog()

viz = Visualizer(width, height, data_height)


def main():
    iteration_count = 0
    running = True
    t = 0
    config = Config()
    config.camera_width = width
    config.camera_height = height
    config.p_noise_level = noise
    config.v_noise_level = noise
    config.attack_power = attack_power
    for i in range(good_count, len(w.birds)):
        config.intruders.append(w.birds[i])
    w.change_noise(config.p_noise_level, config.v_noise_level)
    avg_fr = 0
    count = 0
    total = 0

    config.target = pygame.Vector2(0.0, -2)

    config.camera_offset = pygame.Vector2(0, 0)
    config.camera_zoom = 20
    config.watchdog = watchdog

    w.target = config.target
    w.update(0.02)
    # intruder.createAttackFormation(w, config.camera + pygame.Vector2(width / 2, height), config)
    config.pause = True


    while running:
        # calculate delta t (time since last frame)
        # variable frame-rate is useful for visualization
        # for pure simulation (no graphics), this is irrelevant
        (dt, count, total, avg_fr, t) = get_dt(count, total, avg_fr, t)

        # print(dt)
        # ignore frames that are too 'long'
        # if not, birds may suddenly 'skip' long distances
        # if dt > 50:
        #     print('whoops. That frame took ' + str(dt) + ' ms')
        # continue

        # the exit event is needed to stop this while loop
        # all other events are handled in handle_event()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
                pygame.quit()
                sys.exit()
            else:
                handle_event(event, w, config)

        handle_inputs(config)

        # make world update itself one time-step
        watchdog.watch(w)
        if not config.pause:
            print(iteration_count)
            w.target = config.target
            w.shepherd_target = config.shepherd_target

            w.update(0.05)
            # w.update(0.5)
            config.iteration_count += 1

        # if not watchdog.connected:
        #     print("disconnected")

        viz.draw(w, avg_fr, config, False)
        iteration_count += 1
        # config.pause = True

def set_target(m_pos, w):
    w.targets.append(pygame.Vector2(int(m_pos[0]), int(m_pos[1])))


def get_dt(count, total, avg_fr, t):
    new_t = pygame.time.get_ticks()
    dt = new_t - t
    t = new_t

    total += dt
    count += 1
    if count == 5:
        avg_fr = total/count
        count = 0
        total = 0
    return dt, count, total, avg_fr, t


main()
