import pygame

class Bird:
    max_speed = 10
    max_speed_squared = max_speed**2

    n_distance = 0.0
    n_count = 0

    # construct a single bird
    def __init__(self, x, y, bird_id, p_std=1, v_std=1):
        self.p = pygame.Vector2(x, y)
        self.v = pygame.Vector2(0, 0)
        self.a = pygame.Vector2(0, 0)
        self.id = bird_id

        self.neighbours = []

        self.p_measurements = []
        self.p_inferred = []
        self.v_measurements = []
        self.p_std = p_std
        self.v_std = v_std

        self.v_inferred = []

        self.old_p = self.p
        self.old_v = self.v

        self.marked = False
        self.intruder = False

        self.rsum = pygame.Vector2(0,0)
        self.rss = 0.0
        self.error = 0.0

    def calculate_a(self, world, p, v, p_measurements, v_measurements):
        return pygame.Vector2(0, 0)

    def calculate_v(self, a, delta):
        v = pygame.Vector2(self.old_v)

        v += a*delta

        if v.length_squared() > Bird.max_speed_squared:
            v = v.normalize()*Bird.max_speed

        return v

    @staticmethod
    def calculate_r(w, bird, delta):

        a = w.birds[0].calculate_a(w, bird.p, bird.old_v, bird.p_inferred, bird.v_inferred)
        v = bird.calculate_v(a, delta)
        r = bird.v-v

        return r


    def update_p(self, dt):
        self.old_p = pygame.Vector2(self.p)
        self.p += self.v*dt


    # add generated noise to distance and velocity measuremnents
    def update_measurements(self, world):
        self.p_measurements = [pygame.Vector2(0,0)]*len(self.neighbours)
        self.v_measurements = [pygame.Vector2(0,0)]*len(self.neighbours)
        for i in range(0, len(self.neighbours)):
            n = world.birds[self.neighbours[i]]
            distance = (n.p - self.p)

            p_error = pygame.Vector2(world.p_errors[self.id][i*2], world.p_errors[self.id][(i*2)+1])
            v_error = pygame.Vector2(world.v_errors[self.id][i*2], world.v_errors[self.id][(i*2)+1])

            self.p_measurements[i] = distance + p_error
            self.v_measurements[i] = n.v + v_error
            n.p_inferred.append(-self.p_measurements[i])
            n.v_inferred.append(self.v)





