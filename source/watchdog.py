import source.olafi_flocking
from source import bird
from source.olafi_flocking import OlfatiIntruder

class Watchdog:
    def __init__(self):
        self.connected = False
        self.bad_count = 0
        self.successful_attack = False
        self.success_rate = 0.0

    def watch(self, w):
        # searchset = set()
        # searchset.add(0)
        # foundset = set()
        # foundset.add(0)
        #
        # bad_found = 0
        # while len(searchset) > 0:
        #     current = searchset.pop()
        #     if type(w.birds[current]) != bird.Bird:
        #         bad_found += 1
        #
        #     for n in w.birds[current].neighbours:
        #         if n not in foundset:
        #             searchset.add(n)
        #             foundset.add(n)
        # self.connected = len(foundset)-bad_found == w.good_count

        self.successful_attack = True
        bad_count = 0
        success_count = 0
        for b in w.birds:
            if type(b) == source.olafi_flocking.OlfatiIntruder:
                bad_count += 1
                if b.get_distance_to_target(w) > OlfatiIntruder.target_max_range:
                    self.successful_attack = False
                else:
                    success_count += 1

        if bad_count == 0:
            self.success_rate = 0.0
        else:
            self.success_rate = success_count/bad_count
