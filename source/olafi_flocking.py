import pygame
import random
import math
import numpy as np
from source.bird import Bird

a = 5
b = 5
c = abs(a-b)/math.sqrt(4*a*b)

h = 0.2

epsilon = 0.1

d = 7
# d_alpha = (math.sqrt(1+(epsilon*(d**2)))-1) / epsilon
d_alpha = (1/epsilon) * (math.sqrt(1+(epsilon*(d**2)))-1)

r = d*1.2
# r_alpha = (math.sqrt(1+(epsilon*(r**2)))-1) / epsilon
r_alpha = (1/epsilon) * (math.sqrt(1+(epsilon*(r**2)))-1)

def _bump(z):
    if 0 < z < h:
        return 1
    elif h < z < 1:
        return (1+math.cos(math.pi*(z-h)/(1-h)))/2
    else:
        return 0


def _velocity_matching(v, p_measurements, v_measurements):
    alignment = pygame.math.Vector2(0, 0)
    # for i in range(0, len(bird.neighbours)):
    for i in range(0, len(v_measurements)):
        # b = w.birds[i]
        # if b.id == bird.id:
        #     continue
        # # bump_term = _bump(sigma_norm(bird.p_measurements[i])/r_alpha)
        distance = p_measurements[i]
        if distance.length_squared() > r**2:
            continue
        # distance += pygame.Vector2(np.random.normal(0, w.p_stds[0]), np.random.normal(0, w.p_stds[0]))
        bump_term = _bump(sigma_norm(distance)/r_alpha)
        if bump_term > 0:
            # if not test:
            # #     bird.neighbours.append(i)
            # noise = pygame.Vector2(np.random.normal(0, v_std), np.random.normal(0, 0))

            # v_measurement = b.v + noise
            # b.v_inferred.append(v_measurement)
            alignment += (v_measurements[i] - v)*bump_term
        # if bird.id == 1 and i == 0:
        #     print(bump_term, bird.p_measurements[i], sigma_norm(bird.p_measurements[i], verbose=True))

        # if bird.id == 1:
        #     print(bird.v_measurements[i] - bird.v)

    # if bird.id == 1:
    #     print('velocity matching: ', alignment)
    # bird.intruder_alignment = alignment
    return alignment


def _olafi_gradiant_term(p, p_measurements):
    term = pygame.Vector2(0, 0)
    # other = pygame.Vector2(0, 0)
    # if not test:
    #     bird.marked = False

    for p in p_measurements:
        # if bird.id == 1:
        force = _phi_alpha(sigma_norm(p)) * sigma_gradient(p)
        term += force
        # if p.length_squared() <= 0.01 and not test:
        #     bird.marked = True
            # print('first ', p.length_squared(), force, sigma_gradient(p), sigma_norm(p))
    #         # print('this is the one: ', _phi_alpha(sigma_norm(p), verbose=True))
    #
    # inferred = pygame.Vector2(0, 0)
    # for p in bird.p_inferred:
    #     # if bird.id == 1:
    #     force = _phi_alpha(sigma_norm(p)) * sigma_gradient(p)
    #     term += force
    # bird.intruder_gradiant = inferred

    return term

def _phi_alpha(z, verbose=False):
    if verbose:
        print('_phi_alpha ', z, _phi(z-d_alpha), z/r_alpha,_bump(z/r_alpha))
    return _bump(z/r_alpha)*_phi(z-d_alpha, verbose)


def _phi(z, verbose=False):
    if verbose:
        print('_phi ', z)
    return ((a + b) * (z/math.sqrt(1+(z**2))) + (a-b))/2


def sigma_norm(z, verbose= False):
    if verbose:
        print('sigma_norm', z.length_squared(), math.sqrt(1+(epsilon*(z.length_squared())))-1)
    return (1/epsilon) * (math.sqrt(1+(epsilon*(z.length_squared())))-1)

# print('tester', (1/epsilon) * (math.sqrt(1+(epsilon*(d**2)))-1))


def sigma_gradient(z):
    return z / (math.sqrt(1 + epsilon*z.length_squared()))
    # return z / (1+(epsilon*sigma_norm(z)))


class GammaAgent:
    def __init__(self, p, v):
        self.p = p
        self.r = 0.0
        # self.v = pygame.Vector2(math.cos(self.r), math.sin(self.r))
        self.v = pygame.Vector2(0,-0.50)

    def update(self, world, delta):
        self.r += delta*0.1
        # self.v = pygame.Vector2(math.cos(self.r), math.sin(self.r))
        # self.v.y = self.r
        self.p += self.v*delta*10



# c1 = 0.1
# c2 = 0.01

c1 = 0.02
c2 = 0.1

def _navigational_feedback(gamma_agent, p, v):
    force = -c1*(p-gamma_agent.p) - c2*(v-gamma_agent. v)
    # bird.intruder_feedback = force
    return force

def _seek_center(w, bird):
    direction = pygame.Vector2(w.gamma_agent.p.x, w.flock_center.y)
    direction += bird.formation_target

    direction -= bird.p # min(5, direction.length()*5)
    direction = direction.normalize()*bird.attack_power
    # if bird.split:
    #     dir = (20-l)*1
    #     if (bird.id%2)==1:
    #         dir *= -1
        # direction += pygame.Vector2(dir,0)
    # else:
    #     direction *= 10
    return direction
    # return pygame.Vector2(0,0)


def _olfati_neighbors(bird, w):
    bird.neighbours = []
    for i in range(0, len(w.birds)):
        # b = w.birds[i]
        # if b.id == bird.id:
        #     continue
        # # bump_term = _bump(sigma_norm(bird.p_measurements[i])/r_alpha)
        distance = w.distances[i][bird.id]
        if distance > r:
            continue
        distance = w.birds[i].p-bird.p
        # distance += pygame.Vector2(np.random.normal(0, w.p_stds[0]), np.random.normal(0, w.p_stds[0]))
        bump_term = _bump(sigma_norm(distance)/r_alpha)
        if bump_term > 0:
            # if not test:
            bird.neighbours.append(i)

    # print(len(bird.neighbours))


def create_olfati_bird(x, y, i):
    bird = OlfatiBird(x, y, i)
    bird.v = pygame.Vector2(random.uniform(-2, 2), random.uniform(-1, 1))
    # bird.calculate_a = olafi_calculate_a
    bird.update_neighbors = _olfati_neighbors

    return bird



def olafi_calculate_a(gamma_agent, p, v, p_measurements, v_measurements):
    a = _navigational_feedback(gamma_agent, p, v)
    a += _velocity_matching(v, p_measurements, v_measurements)
    a += _olafi_gradiant_term(p, p_measurements)
    return a

# def olafi_calculate_a(gamma_agent, p, v, p_measurements, v_measurements, test=False):
#     a = _navigational_feedback(gamma_agent, p, v)
#     a += _velocity_matching(v, p_measurements, v_measurements)
#     a += _olafi_gradiant_term(p, p_measurements)
#     return a

def create_selfish_olafi(x, y, i):
    bird = Bird(x, y, i)
    bird.v = pygame.Vector2(random.uniform(-2, 2), random.uniform(-1, 1))

    bird.flocking_rules.append(_intruder_rules)
    bird.flocking_rules.append(_navigational_feedback)
    bird.intruder = True

    return bird

def _intruder_rules(w, bird):
    _velocity_matching(w, bird)
    _olafi_gradiant_term(w, bird)
    return pygame.Vector2(0,0)

# intruder_count = 0
# def create_olfati_intruder(x, y, i, w, formation_target=pygame.Vector2(0,0)):
#     bird = Bird(x, y, i)
#     bird.v = pygame.Vector2(random.uniform(-2, 2), random.uniform(-1, 1))
#     bird.calculate_a = lambda gamma_agent, p, v, p_measurements, v_measurements: olafi_calculate_a(gamma_agent, p, v, p_measurements, v_measurements)+_seek_center(w, bird)
#     bird.formation_target = formation_target
#     bird.update_neighbors = _olfati_neighbors
#     bird.intruder = True
#     bird.split = False
#     return bird

class OlfatiBird(Bird):

    def calculate_a(self, w, p, v, p_measurements, v_measurements, test=False):
        a = _navigational_feedback(w.gamma_agent, p, v)
        a += _velocity_matching(v, p_measurements, v_measurements)
        a += _olafi_gradiant_term(p, p_measurements)
        return a

    def update_neighbors(self, w):
        self.neighbours = []
        for i in range(0, len(w.birds)):
            # b = w.birds[i]
            # if b.id == bird.id:
            #     continue
            # # bump_term = _bump(sigma_norm(bird.p_measurements[i])/r_alpha)
            distance = w.distances[i][self.id]
            if distance > r:
                continue
            distance = w.birds[i].p - self.p
            # distance += pygame.Vector2(np.random.normal(0, w.p_stds[0]), np.random.normal(0, w.p_stds[0]))
            bump_term = _bump(sigma_norm(distance) / r_alpha)
            if bump_term > 0:
                # if not test:
                self.neighbours.append(i)


from enum import Enum
class AttackMode(Enum):
    FIND = 0
    INFILTRATE = 1
    RESET = 2
    SPLIT = 3


class OlfatiIntruder(OlfatiBird):
    count = 0
    target_min_range = 1
    target_max_range = 4

    @staticmethod
    def change_attack_power(power):
        OlfatiIntruder.attack_power = power

    def __init__(self,x, y, bird_id, target, power=10):
        super().__init__(x, y, bird_id, p_std=1, v_std=1)
        self.intruder = True
        self.formation_target = target
        self.split = False
        self.mode = AttackMode.FIND
        self.attack_power = power
        OlfatiIntruder.count += 1


    def update_neighbors(self, w):
        super().update_neighbors(w)
        if len(self.neighbours) > 0 and self.mode == AttackMode.FIND:
            for n in self.neighbours:
                if not w.birds[n].intruder:
                    self.mode = AttackMode.INFILTRATE

    def calculate_a(self, w, p, v, p_measurements, v_measurements, test=False):
        acc = super().calculate_a(w, p, v, p_measurements, v_measurements, test=False)

        target_d = self.get_distance_to_target(w)

        if self.mode == AttackMode.INFILTRATE:
            if target_d < OlfatiIntruder.target_min_range:
                self.mode = AttackMode.RESET
            else:
                acc += _seek_center(w, self)
        elif self.mode == AttackMode.RESET:
            if target_d > OlfatiIntruder.target_max_range:
                self.mode = AttackMode.INFILTRATE
            elif self.rsum.length() > 0.0:
                # acc += -self.rsum.normalize()*0.3
                # acc += _seek_center(w, self)*0.1
                pass
        return acc

    def get_current_target(self, w):
        direction = pygame.Vector2(w.gamma_agent.p.x, w.flock_center.y)
        return direction + self.formation_target

    def get_distance_to_target(self, w):
        return (self.get_current_target(w) - self.p).length()