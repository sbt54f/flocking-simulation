import numpy as np
from source import bird
from source.bird import Bird
import pygame
from source.olafi_flocking import GammaAgent

class World:
    def __init__(self):
        self.birds = []
        self.distances = []
        self.p_errors = []
        self.v_errors = []
        self.v_sum = 0.0

        self.good_count = 0
        self.bad_count = 0
        self.gamma_agent = GammaAgent(pygame.Vector2(50, 0), pygame.Vector2(0, 0))
        self.flock_center = pygame.Vector2(0, 0)


    def update(self, dt):
        self.v_sum = pygame.Vector2(0.0, 0.0 )

        self.flock_center = pygame.Vector2(0.0, 0.0)
        for bird in self.birds:
            self.flock_center += bird.p
        if len(self.birds) > 0:
            self.flock_center /= len(self.birds)

        self.gamma_agent.update(self, dt)

        self.calculate_distances()

        for bird in self.birds:
            bird.p_inferred = []
            bird.v_inferred = []

        for bird in self.birds:
            # bird.update_neighbours(self)
            bird.update_measurements(self)
            bird.update_neighbors(self)
            bird.old_v = pygame.Vector2(bird.v)

        for bird in self.birds:
            new_a = bird.calculate_a(self, bird.p, bird.old_v, bird.p_measurements, bird.v_measurements)
            bird.a = new_a
            new_v = bird.calculate_v(new_a, dt)
            bird.v = new_v
            r = bird.calculate_r(self, bird, dt)
            bird.error = r.length()
            bird.rsum += r
            bird.rss += r.length_squared()
            self.v_sum += bird.v

        for bird in self.birds:
            bird.update_p(dt)

        # print((self.v_sum/len(self.birds)).length())

    def calculate_distances(self):
        if len(self.distances) < len(self.birds):
            self.distances = np.zeros((len(self.birds), len(self.birds)))

        for i in range(0, len(self.birds)):
            for j in range(0, len(self.birds)):
                distance = self.birds[i].p - self.birds[j].p
                self.distances[i][j] = distance.length()



        if len(self.birds) > 1:

            self.p_errors = np.random.normal(0, 1, size=(len(self.birds), len(self.birds)*2))
            self.p_errors = np.multiply(self.p_errors, np.array(self.p_stds)[:, np.newaxis])
            self.v_errors = np.random.normal(0, 1, size=(len(self.birds), len(self.birds)*2))
            self.v_errors = np.multiply(self.v_errors, np.array(self.v_stds)[:, np.newaxis])

    def addBird(self, b):
        b.id = len(self.birds)
        if len(self.birds) == 0:
            self.birds = [b]
            self.p_stds = [0.0]
            self.v_stds = [0.0]
        else:
            self.birds += [b]
            self.p_stds = np.append(self.p_stds, 0.0)
            self.v_stds = np.append(self.v_stds, 0.0)
        #
        # b.p_measurements = [0.0] * 10
        # b.v_measurements = [0.0] * 10

        if b.intruder:
            self.bad_count += 1
        else:
            self.good_count += 1

    def change_noise(self, p_std, v_std):
        self.p_stds = np.full(len(self.p_stds), p_std)
        self.v_stds = np.full(len(self.p_stds), v_std)

    def reset_error_sums(self):
        for b in self.birds:
            b.RSUM = pygame.Vector2(0,0)
            b.rse = 0.0
